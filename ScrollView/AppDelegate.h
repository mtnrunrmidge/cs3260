//
//  AppDelegate.h
//  ScrollView
//
//  Created by Spencer Warner on 8/30/17.
//  Copyright © 2017 Margaret Schroeder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

