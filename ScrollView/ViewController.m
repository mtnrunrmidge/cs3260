//
//  ViewController.m
//  ScrollView
//
//  Created by Margaret Schroeder on 8/30/17.
//  Copyright © 2017 Margaret Schroeder. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //Blue square 0,0
    UIScrollView* scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    scrollView.backgroundColor = [UIColor blueColor];
    [self.view addSubview:scrollView];
    
    [scrollView setContentSize:CGSizeMake(self.view.frame.size.width*3, self.view.frame.size.height * 3)];
    
    //Red square 0,1
    UIView* redView = [[UIView alloc]initWithFrame:CGRectMake(scrollView.frame.size.width, 0, scrollView.frame.size.width, scrollView.frame.size.height)];
    
    redView.backgroundColor = [UIColor redColor];
    [scrollView addSubview:redView];
    
    //yellow square 0,2
    UIView* yellowView = [[UIView alloc]initWithFrame:CGRectMake(scrollView.frame.size.width * 2, 0, scrollView.frame.size.width, scrollView.frame.size.height)];
    
    yellowView.backgroundColor = [UIColor yellowColor];
    [scrollView addSubview:yellowView];

    //orange square 1,0
    UIView* orangeView = [[UIView alloc]initWithFrame:CGRectMake(0, scrollView.frame.size.height, scrollView.frame.size.width, scrollView.frame.size.height)];
    
    orangeView.backgroundColor = [UIColor orangeColor];
    [scrollView addSubview:orangeView];
    
    //black square 1,1
    UIView* blackView = [[UIView alloc]initWithFrame:CGRectMake(scrollView.frame.size.width, scrollView.frame.size.height, scrollView.frame.size.width, scrollView.frame.size.height)];
    
    blackView.backgroundColor = [UIColor blackColor];
    [scrollView addSubview:blackView];
    
    //cyan square 1,2
    UIView* cyanView = [[UIView alloc]initWithFrame:CGRectMake(scrollView.frame.size.width, scrollView.frame.size.height * 2, scrollView.frame.size.width, scrollView.frame.size.height)];
    
    cyanView.backgroundColor = [UIColor cyanColor];
    [scrollView addSubview:cyanView];
    
    //white square 2,0
    UIView* whiteView = [[UIView alloc]initWithFrame:CGRectMake(0, scrollView.frame.size.height * 2, scrollView.frame.size.width, scrollView.frame.size.height)];
    
    whiteView.backgroundColor = [UIColor whiteColor];
    [scrollView addSubview:whiteView];
    
    //brown square 2,1
    UIView* brownView = [[UIView alloc]initWithFrame:CGRectMake(scrollView.frame.size.width, scrollView.frame.size.height * 2, scrollView.frame.size.width, scrollView.frame.size.height)];
    
    brownView.backgroundColor = [UIColor brownColor];
    [scrollView addSubview:brownView];
    
    //green square 2,2
    UIView* greenView = [[UIView alloc]initWithFrame:CGRectMake(scrollView.frame.size.width * 2,scrollView.frame.size.height * 2, scrollView.frame.size.width, scrollView.frame.size.height)];
    
    greenView.backgroundColor = [UIColor greenColor];
    [scrollView addSubview:greenView];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
